package com.ppm.project.manuscripts.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ppm.project.manuscripts.domain.PpmArticleRef;

import java.util.List;

/**
 * 文章引用Mapper接口
 * 
 * @author ppm
 * @date 2020-04-03
 */
public interface PpmArticleRefMapper  extends BaseMapper<PpmArticleRef>
{
    /**
     * 查询文章引用
     * 
     * @param id 文章引用ID
     * @return 文章引用
     */
    public PpmArticleRef selectPpmArticleRefById(Long id);

    /**
     * 查询文章引用列表
     * 
     * @param ppmArticleRef 文章引用
     * @return 文章引用集合
     */
    public List<PpmArticleRef> selectPpmArticleRefList(PpmArticleRef ppmArticleRef);

    /**
     * 新增文章引用
     * 
     * @param ppmArticleRef 文章引用
     * @return 结果
     */
    public int insertPpmArticleRef(PpmArticleRef ppmArticleRef);

    /**
     * 修改文章引用
     * 
     * @param ppmArticleRef 文章引用
     * @return 结果
     */
    public int updatePpmArticleRef(PpmArticleRef ppmArticleRef);

    /**
     * 删除文章引用
     * 
     * @param id 文章引用ID
     * @return 结果
     */
    public int deletePpmArticleRefById(Long id);

    /**
     * 批量删除文章引用
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePpmArticleRefByIds(Long[] ids);
}
