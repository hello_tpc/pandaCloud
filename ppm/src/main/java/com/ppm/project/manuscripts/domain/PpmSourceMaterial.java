package com.ppm.project.manuscripts.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ppm.framework.aspectj.lang.annotation.Excel;
import com.ppm.framework.web.domain.server.NonEmptyBaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 共享素材管理对象 ppm_source_material
 * 
 * @author ppm
 * @date 2020-08-11
 */
public class PpmSourceMaterial extends NonEmptyBaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 文章Id */
    @Excel(name = "文章Id")
    private String articleId;

    /** 素材类型,video视频、audio音频、img图片 */
    @Excel(name = "素材类型,video视频、audio音频、img图片")
    private String type;

    /** 素材资源地址 */
    @Excel(name = "素材资源地址")
    private String materialUrl;

    /** 素材名称 */
    @Excel(name = "素材名称")
    private String materialName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setMaterialUrl(String materialUrl) 
    {
        this.materialUrl = materialUrl;
    }

    public String getMaterialUrl() 
    {
        return materialUrl;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("articleId", getArticleId())
            .append("type", getType())
            .append("materialUrl", getMaterialUrl())
            .append("materialName", getMaterialName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
