package com.ppm.project.manuscripts.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ppm.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.project.manuscripts.domain.PpmSourceMaterial;
import com.ppm.project.manuscripts.service.IPpmSourceMaterialService;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;
import com.ppm.common.utils.poi.ExcelUtil;
import com.ppm.framework.web.page.TableDataInfo;

/**
 * 共享素材管理Controller
 * 
 * @author ppm
 * @date 2020-08-11
 */
@RestController
@RequestMapping("/manuscripts/sharematerial")
public class PpmSourceMaterialController extends BaseController
{
    @Autowired
    private IPpmSourceMaterialService ppmSourceMaterialService;

    /**
     * 查询共享素材管理列表
     */
//    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:list')")
    @GetMapping("/list")
    public TableDataInfo list(PpmSourceMaterial ppmSourceMaterial)
    {
        startPage();
        List<PpmSourceMaterial> list = ppmSourceMaterialService.selectPpmSourceMaterialList(ppmSourceMaterial);
        return getDataTable(list);
    }

    /**
     * 导出共享素材管理列表
     */
//    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:export')")
    @Log(title = "共享素材管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PpmSourceMaterial ppmSourceMaterial)
    {
        List<PpmSourceMaterial> list = ppmSourceMaterialService.selectPpmSourceMaterialList(ppmSourceMaterial);
        ExcelUtil<PpmSourceMaterial> util = new ExcelUtil<PpmSourceMaterial>(PpmSourceMaterial.class);
        return util.exportExcel(list, "sharematerial");
    }

    /**
     * 获取共享素材管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ppmSourceMaterialService.selectPpmSourceMaterialById(id));
    }


    /**
     * 手动同步文章的共享素材
     */
//    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:query')")
    @PostMapping(value = "/syncMaterial")
    public AjaxResult syncMaterial(@RequestBody JSONObject jsonObject)
    {

        return toAjax(ppmSourceMaterialService.syncMaterial(jsonObject.getString("articleId"))?1:0);

    }

    /**
     * 新增共享素材管理
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:add')")
    @Log(title = "共享素材管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PpmSourceMaterial ppmSourceMaterial)
    {
        ppmSourceMaterial.setCreateBy(SecurityUtils.getUsername());
        ppmSourceMaterial.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(ppmSourceMaterialService.insertPpmSourceMaterial(ppmSourceMaterial));
    }

    /**
     * 修改共享素材管理
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:edit')")
    @Log(title = "共享素材管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PpmSourceMaterial ppmSourceMaterial)
    {
        ppmSourceMaterial.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(ppmSourceMaterialService.updatePpmSourceMaterial(ppmSourceMaterial));
    }

    /**
     * 删除共享素材管理
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharematerial:remove')")
    @Log(title = "共享素材管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ppmSourceMaterialService.deletePpmSourceMaterialByIds(ids));
    }
}
