package com.ppm.project.manuscripts.domain;

import com.ppm.framework.aspectj.lang.annotation.Excel;
import com.ppm.framework.web.domain.server.NonEmptyBaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 来源站点对象 ppm_site_source
 *
 * @author ppm
 * @date 2020-04-03
 */
public class PpmSiteSource extends NonEmptyBaseEntity

{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 上级编码 */
    @Excel(name = "上级编码" ,prompt = "如果是根站点默认为0")
    private String parentSiteCode;

    /** 源编码 */
    @Excel(name = "源编码")
    private String siteCode;

    /** 源名称 */
    @Excel(name = "源名称")
    private String siteName;

    /** 源全称 */
    @Excel(name = "源全称")
    private String siteFullname;

    /** 站点url */
    @Excel(name = "站点url")
    private String siteUrl;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    private String editor;

    /** 积分 */
//    @Excel(name = "积分")
    private Long integral;

    /** 是否允许访问区县共享库 */
//    @Excel(name = "是否允许访问区县共享库",prompt = "1代表允许,0代表不允许")
    private Integer shareFlag;


    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSiteCode(String siteCode)
    {
        this.siteCode = siteCode;
    }

    public String getSiteCode()
    {
        return siteCode;
    }
    public void setParentSiteCode(String parentSiteCode)
    {
        this.parentSiteCode = parentSiteCode;
    }

    public String getParentSiteCode()
    {
        return parentSiteCode;
    }
    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getSiteName()
    {
        return siteName;
    }
    public void setSiteFullname(String siteFullname)
    {
        this.siteFullname = siteFullname;
    }

    public String getSiteFullname()
    {
        return siteFullname;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setEditor(String editor)
    {
        this.editor = editor;
    }

    public String getEditor()
    {
        return editor;
    }
    public void setIntegral(Long integral)
    {
        this.integral = integral;
    }

    public Long getIntegral()
    {
        return integral;
    }
    public void setShareFlag(Integer shareFlag)
    {
        this.shareFlag = shareFlag;
    }

    public Integer getShareFlag()
    {
        return shareFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("siteCode", getSiteCode())
                .append("parentSiteCode", getParentSiteCode())
                .append("siteName", getSiteName())
                .append("siteFullname", getSiteFullname())
                .append("siteUrl", getSiteUrl())
                .append("phone", getPhone())
                .append("editor", getEditor())
                .append("integral", getIntegral())
                .append("shareFlag", getShareFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}