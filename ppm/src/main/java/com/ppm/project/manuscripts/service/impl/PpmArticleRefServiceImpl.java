package com.ppm.project.manuscripts.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ppm.common.constant.ErrorConstants;
import com.ppm.common.exception.CustomException;
import com.ppm.common.utils.DateUtils;
import com.ppm.project.manuscripts.domain.PpmArticleRef;
import com.ppm.project.manuscripts.mapper.PpmArticleRefMapper;
import com.ppm.project.manuscripts.service.IPpmArticleRefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章引用Service业务层处理
 * 
 * @author ppm
 * @date 2020-04-03
 */
@Service
public class PpmArticleRefServiceImpl extends ServiceImpl<PpmArticleRefMapper,PpmArticleRef> implements IPpmArticleRefService
{
    @Autowired
    private PpmArticleRefMapper ppmArticleRefMapper;

    /**
     * 查询文章引用
     * 
     * @param id 文章引用ID
     * @return 文章引用
     */
    @Override
    public PpmArticleRef selectPpmArticleRefById(Long id)
    {
        return ppmArticleRefMapper.selectPpmArticleRefById(id);
    }

    /**
     * 查询文章引用列表
     * 
     * @param ppmArticleRef 文章引用
     * @return 文章引用
     */
    @Override
    public List<PpmArticleRef> selectPpmArticleRefList(PpmArticleRef ppmArticleRef)
    {
        return ppmArticleRefMapper.selectPpmArticleRefList(ppmArticleRef);
    }

    /**
     * 新增文章引用
     * 
     * @param ppmArticleRef 文章引用
     * @return 结果
     */
    @Override
    public int insertPpmArticleRef(PpmArticleRef ppmArticleRef)
    {
        List<PpmArticleRef> existList = ppmArticleRefMapper.selectPpmArticleRefList(ppmArticleRef);
        if(existList == null || existList.size() <1){

            ppmArticleRef.setCreateTime(DateUtils.getNowDate());
            return ppmArticleRefMapper.insertPpmArticleRef(ppmArticleRef);
        }

        return 1;
    }

    /**
     * 修改文章引用
     * 
     * @param ppmArticleRef 文章引用
     * @return 结果
     */
    @Override
    public int updatePpmArticleRef(PpmArticleRef ppmArticleRef)
    {
        ppmArticleRef.setUpdateTime(DateUtils.getNowDate());
        return ppmArticleRefMapper.updatePpmArticleRef(ppmArticleRef);
    }

    /**
     * 批量删除文章引用
     * 
     * @param ids 需要删除的文章引用ID
     * @return 结果
     */
    @Override
    public int deletePpmArticleRefByIds(Long[] ids)
    {
        return ppmArticleRefMapper.deletePpmArticleRefByIds(ids);
    }

    /**
     * 删除文章引用信息
     * 
     * @param id 文章引用ID
     * @return 结果
     */
    @Override
    public int deletePpmArticleRefById(Long id)
    {
        return ppmArticleRefMapper.deletePpmArticleRefById(id);
    }
}
