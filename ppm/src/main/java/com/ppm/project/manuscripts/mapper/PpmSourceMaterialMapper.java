package com.ppm.project.manuscripts.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ppm.project.manuscripts.domain.PpmSourceMaterial;

/**
 * 共享素材管理Mapper接口
 * 
 * @author ppm
 * @date 2020-08-11
 */
public interface PpmSourceMaterialMapper  extends BaseMapper<PpmSourceMaterial>
{
    /**
     * 查询共享素材管理
     * 
     * @param id 共享素材管理ID
     * @return 共享素材管理
     */
    public PpmSourceMaterial selectPpmSourceMaterialById(Long id);

    /**
     * 查询共享素材管理列表
     * 
     * @param ppmSourceMaterial 共享素材管理
     * @return 共享素材管理集合
     */
    public List<PpmSourceMaterial> selectPpmSourceMaterialList(PpmSourceMaterial ppmSourceMaterial);

    /**
     * 新增共享素材管理
     * 
     * @param ppmSourceMaterial 共享素材管理
     * @return 结果
     */
    public int insertPpmSourceMaterial(PpmSourceMaterial ppmSourceMaterial);

    /**
     * 修改共享素材管理
     * 
     * @param ppmSourceMaterial 共享素材管理
     * @return 结果
     */
    public int updatePpmSourceMaterial(PpmSourceMaterial ppmSourceMaterial);

    /**
     * 删除共享素材管理
     * 
     * @param id 共享素材管理ID
     * @return 结果
     */
    public int deletePpmSourceMaterialById(Long id);

    /**
     * 批量删除共享素材管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePpmSourceMaterialByIds(Long[] ids);
}
