package com.ppm.project.manuscripts.domain;

import com.ppm.framework.aspectj.lang.annotation.Excel;
import com.ppm.framework.web.domain.server.NonEmptyBaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 文章引用对象 ppm_article_ref
 * 
 * @author ppm
 * @date 2020-04-03
 */
public class PpmArticleRef extends NonEmptyBaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 文章来源区县代码 */
    @Excel(name = "文章来源区县代码")
    private String sourceFrom;

    /** 引用文章区县代码 */
    @Excel(name = "引用文章区县代码")
    private String sourceTo;

    /** 引用文章ID */
    @Excel(name = "引用文章ID")
    private String refArticleid;

    /** 市区县名称 */
    private String sourceToName;

    /** 引用次数 */
    private Integer refNum = 1;

    /** 联系电话 */
    private String phone;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSourceFrom(String sourceFrom) 
    {
        this.sourceFrom = sourceFrom;
    }

    public String getSourceFrom() 
    {
        return sourceFrom;
    }
    public void setSourceTo(String sourceTo) 
    {
        this.sourceTo = sourceTo;
    }

    public String getSourceTo() 
    {
        return sourceTo;
    }
    public void setRefArticleid(String refArticleid) 
    {
        this.refArticleid = refArticleid;
    }

    public String getRefArticleid() 
    {
        return refArticleid;
    }

    public String getSourceToName() { return sourceToName; }

    public void setSourceToName(String sourceToName) { this.sourceToName = sourceToName; }

    public Integer getRefNum() { return refNum; }

    public void setRefNum(Integer refNum) { this.refNum = refNum; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sourceFrom", getSourceFrom())
            .append("sourceTo", getSourceTo())
            .append("refArticleid", getRefArticleid())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
