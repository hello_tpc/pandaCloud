package com.ppm.project.manuscripts.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ppm.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ppm.project.manuscripts.mapper.ShareManuscriptsMapper;
import com.ppm.project.manuscripts.domain.ShareManuscripts;
import com.ppm.project.manuscripts.service.IShareManuscriptsService;

/**
 * 共享稿件管理Service业务层处理
 * 
 * @author ppm
 * @date 2020-04-03
 */
@Service
public class ShareManuscriptsServiceImpl extends ServiceImpl<ShareManuscriptsMapper,ShareManuscripts> implements IShareManuscriptsService
{
    @Autowired
    private ShareManuscriptsMapper shareManuscriptsMapper;

    /**
     * 查询共享稿件管理
     * 
     * @param id 共享稿件管理ID
     * @return 共享稿件管理
     */
    @Override
    public ShareManuscripts selectShareManuscriptsById(Long id)
    {
        return shareManuscriptsMapper.selectShareManuscriptsById(id);
    }

    /**
     * 查询共享稿件管理列表
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 共享稿件管理
     */
    @Override
    public List<ShareManuscripts> selectShareManuscriptsList(ShareManuscripts shareManuscripts)
    {
        return shareManuscriptsMapper.selectShareManuscriptsList(shareManuscripts);
    }

    /**
     * 新增共享稿件管理
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 结果
     */
    @Override
    public int insertShareManuscripts(ShareManuscripts shareManuscripts)
    {
        shareManuscripts.setCreateTime(DateUtils.getNowDate());
        return shareManuscriptsMapper.insertShareManuscripts(shareManuscripts);
    }

    /**
     * 修改共享稿件管理
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 结果
     */
    @Override
    public int updateShareManuscripts(ShareManuscripts shareManuscripts)
    {
        shareManuscripts.setUpdateTime(DateUtils.getNowDate());
        return shareManuscriptsMapper.updateShareManuscripts(shareManuscripts);
    }

    /**
     * 批量删除共享稿件管理
     * 
     * @param ids 需要删除的共享稿件管理ID
     * @return 结果
     */
    @Override
    public int deleteShareManuscriptsByIds(Long[] ids)
    {
        return shareManuscriptsMapper.deleteShareManuscriptsByIds(ids);
    }

    /**
     * 删除共享稿件管理信息
     * 
     * @param id 共享稿件管理ID
     * @return 结果
     */
    @Override
    public int deleteShareManuscriptsById(Long id)
    {
        return shareManuscriptsMapper.deleteShareManuscriptsById(id);
    }
}
