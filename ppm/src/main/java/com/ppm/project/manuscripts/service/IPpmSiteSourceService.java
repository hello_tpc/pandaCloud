package com.ppm.project.manuscripts.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ppm.project.manuscripts.domain.PpmSiteSource;

/**
 * 来源站点Service接口
 * 
 * @author ppm
 * @date 2020-04-02
 */
public interface IPpmSiteSourceService extends IService<PpmSiteSource>
{
    /**
     * 查询来源站点
     * 
     * @param siteSourceId 来源站点ID
     * @return 来源站点
     */
    public PpmSiteSource selectPpmSiteSourceById(Long siteSourceId);

    /**
     * 查询来源站点列表
     * 
     * @param ppmSiteSource 来源站点
     * @return 来源站点集合
     */
    public List<PpmSiteSource> selectPpmSiteSourceList(PpmSiteSource ppmSiteSource);

    /**
     * 新增来源站点
     * 
     * @param ppmSiteSource 来源站点
     * @return 结果
     */
    public int insertPpmSiteSource(PpmSiteSource ppmSiteSource);

    /**
     * 修改来源站点
     * 
     * @param ppmSiteSource 来源站点
     * @return 结果
     */
    public int updatePpmSiteSource(PpmSiteSource ppmSiteSource);

    /**
     * 批量删除来源站点
     * 
     * @param siteSourceIds 需要删除的来源站点ID
     * @return 结果
     */
    public int deletePpmSiteSourceByIds(Long[] siteSourceIds);

    /**
     * 删除来源站点信息
     * 
     * @param siteSourceId 来源站点ID
     * @return 结果
     */
    public int deletePpmSiteSourceById(Long siteSourceId);

    /**
     * 获取所有站点信息
     * @return
     */
    public Map<String,List<String>> getAllSite();


    /**
     *
     * excel导入站点信息
     *
     */
    public String importSiteSource(List<PpmSiteSource> siteList, Boolean isUpdateSupport, String operName);

}
