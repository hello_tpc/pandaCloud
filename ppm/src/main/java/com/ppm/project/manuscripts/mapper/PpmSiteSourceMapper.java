package com.ppm.project.manuscripts.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ppm.project.manuscripts.domain.PpmSiteSource;

/**
 * 来源站点Mapper接口
 * 
 * @author ppm
 * @date 2020-04-02
 */
public interface PpmSiteSourceMapper extends BaseMapper<PpmSiteSource>
{
    /**
     * 查询来源站点
     * 
     * @param siteSourceId 来源站点ID
     * @return 来源站点
     */
    public PpmSiteSource selectPpmSiteSourceById(Long siteSourceId);

    /**
     * 查询来源站点列表
     * 
     * @param ppmSiteSource 来源站点
     * @return 来源站点集合
     */
    public List<PpmSiteSource> selectPpmSiteSourceList(PpmSiteSource ppmSiteSource);

    /**
     * 新增来源站点
     * 
     * @param ppmSiteSource 来源站点
     * @return 结果
     */
    public int insertPpmSiteSource(PpmSiteSource ppmSiteSource);

    /**
     * 修改来源站点
     * 
     * @param ppmSiteSource 来源站点
     * @return 结果
     */
    public int updatePpmSiteSource(PpmSiteSource ppmSiteSource);

    /**
     * 更新积分
     *
     * @param ppmSiteSource
     * @return 结果
     */
    public int updateIntegral(PpmSiteSource ppmSiteSource);

    /**
     * 删除来源站点
     * 
     * @param siteSourceId 来源站点ID
     * @return 结果
     */
    public int deletePpmSiteSourceById(Long siteSourceId);

    /**
     * 批量删除来源站点
     * 
     * @param siteSourceIds 需要删除的数据ID
     * @return 结果
     */
    public int deletePpmSiteSourceByIds(Long[] siteSourceIds);

}
