package com.ppm.project.manuscripts.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.project.manuscripts.domain.ShareManuscripts;
import com.ppm.project.manuscripts.service.IShareManuscriptsService;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;
import com.ppm.common.utils.poi.ExcelUtil;
import com.ppm.framework.web.page.TableDataInfo;

/**
 * 共享稿件管理Controller
 * 
 * @author ppm
 * @date 2020-04-03
 */
@Api(description = "省平台共享稿件接口相关",value = "省平台共享稿件接口",tags = {"省平台共享稿件接口"})
@RestController
@RequestMapping("/manuscripts/sharemanuscripts")
public class ShareManuscriptsController extends BaseController
{
    /**
     * 查询所有共享稿件列表
     */
    @ApiOperation("查询所有共享稿件列表")
//    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShareManuscripts shareManuscripts)
    {
        startPage();
        List<ShareManuscripts> list = shareManuscriptsService.selectShareManuscriptsList(shareManuscripts);
        return getDataTable(list);
    }

    @Autowired
    private IShareManuscriptsService shareManuscriptsService;

    /**
     * 导出共享稿件管理列表
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:export')")
    @Log(title = "共享稿件管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ShareManuscripts shareManuscripts)
    {
        List<ShareManuscripts> list = shareManuscriptsService.selectShareManuscriptsList(shareManuscripts);
        ExcelUtil<ShareManuscripts> util = new ExcelUtil<ShareManuscripts>(ShareManuscripts.class);
        return util.exportExcel(list, "sharemanuscripts");
    }

    /**
     * 获取共享稿件管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(shareManuscriptsService.selectShareManuscriptsById(id));
    }

    /**
     * 新增共享稿件管理
     */
    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:add')")
    @Log(title = "共享稿件管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ShareManuscripts shareManuscripts)
    {
        return toAjax(shareManuscriptsService.insertShareManuscripts(shareManuscripts));
    }

    /**
     * 修改共享稿件管理
     */
    @ApiOperation("修改共享稿件信息")
    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:edit')")
    @Log(title = "共享稿件管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ShareManuscripts shareManuscripts)
    {
        return toAjax(shareManuscriptsService.updateShareManuscripts(shareManuscripts));
    }

    /**
     * 删除共享稿件管理
     */
    @ApiOperation("删除共享稿件")
    @PreAuthorize("@ss.hasPermi('manuscripts:sharemanuscripts:remove')")
    @Log(title = "共享稿件管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shareManuscriptsService.deleteShareManuscriptsByIds(ids));
    }
}
