package com.ppm.project.manuscripts.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.project.manuscripts.domain.PpmArticleRef;
import com.ppm.project.manuscripts.service.IPpmArticleRefService;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;
import com.ppm.common.utils.poi.ExcelUtil;
import com.ppm.framework.web.page.TableDataInfo;

/**
 * 文章引用Controller
 * 
 * @author ppm
 * @date 2020-04-03
 */
@RestController
@RequestMapping("/manuscripts/ref")
public class PpmArticleRefController extends BaseController
{
    @Autowired
    private IPpmArticleRefService ppmArticleRefService;

    /**
     * 查询文章引用列表
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:list')")
    @GetMapping("/list")
    public TableDataInfo list(PpmArticleRef ppmArticleRef)
    {
        startPage();
        List<PpmArticleRef> list = ppmArticleRefService.selectPpmArticleRefList(ppmArticleRef);
        return getDataTable(list);
    }

    /**
     * 导出文章引用列表
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:export')")
    @Log(title = "文章引用", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PpmArticleRef ppmArticleRef)
    {
        List<PpmArticleRef> list = ppmArticleRefService.selectPpmArticleRefList(ppmArticleRef);
        ExcelUtil<PpmArticleRef> util = new ExcelUtil<PpmArticleRef>(PpmArticleRef.class);
        return util.exportExcel(list, "ref");
    }

    /**
     * 获取文章引用详细信息
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ppmArticleRefService.selectPpmArticleRefById(id));
    }

    /**
     * 新增文章引用
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:add')")
    @Log(title = "文章引用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PpmArticleRef ppmArticleRef)
    {
        return toAjax(ppmArticleRefService.insertPpmArticleRef(ppmArticleRef));
    }

    /**
     * 修改文章引用
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:edit')")
    @Log(title = "文章引用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PpmArticleRef ppmArticleRef)
    {
        return toAjax(ppmArticleRefService.updatePpmArticleRef(ppmArticleRef));
    }

    /**
     * 删除文章引用
     */
    //@PreAuthorize("@ss.hasPermi('manuscripts:ref:remove')")
    @Log(title = "文章引用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ppmArticleRefService.deletePpmArticleRefByIds(ids));
    }
}
