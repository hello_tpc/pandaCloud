package com.ppm.project.manuscripts.controller;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ppm.common.utils.SecurityUtils;
import com.ppm.common.utils.ServletUtils;
import com.ppm.framework.security.LoginUser;
import com.ppm.framework.security.service.TokenService;
import com.ppm.framework.web.page.TableDataInfo;
import com.ppm.project.manuscripts.domain.PpmSiteSource;
import com.ppm.project.manuscripts.service.IPpmSiteSourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;
import com.ppm.common.utils.poi.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;

/**
 * 来源站点Controller
 * 
 * @author ppm
 * @date 2020-04-02
 */
@Api(description = "省平台来源站点接口相关",value = "省平台来源站点接口",tags = {"省平台来源站点接口"})
@RestController
@RequestMapping("/manuscripts/source")
public class PpmSiteSourceController extends BaseController
{
    @Autowired
    private IPpmSiteSourceService ppmSiteSourceService;

    @Autowired
    private TokenService tokenService;


    /**
     * 查询来源站点列表
     */
    @ApiOperation("省平台查询来源站点列表")
//    @PreAuthorize("@ss.hasPermi('system:source:list')")
    @GetMapping("/list")
    public TableDataInfo list(PpmSiteSource ppmSiteSource)
    {
        startPage();
        List<PpmSiteSource> list = ppmSiteSourceService.selectPpmSiteSourceList(ppmSiteSource);
        return getDataTable(list);
    }



    @GetMapping("/getIntegralBySiteCode")
    public Long getIntegralBySiteCode(PpmSiteSource srcPpmSiteSource)
    {
        PpmSiteSource retPpmSiteSource = ppmSiteSourceService.getOne(new QueryWrapper<PpmSiteSource>()
                .eq("site_code" , srcPpmSiteSource.getSiteCode()));

        return retPpmSiteSource != null?retPpmSiteSource.getIntegral():0;
    }


    @Log(title = "来源站点", businessType = BusinessType.IMPORT)
//    @PreAuthorize("@ss.hasPermi('system:user:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<PpmSiteSource> util = new ExcelUtil<PpmSiteSource>(PpmSiteSource.class);
        List<PpmSiteSource> siteList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = ppmSiteSourceService.importSiteSource(siteList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     *导出Excel模板
     * @return
     */
    @GetMapping("/importTemplate")
    public AjaxResult importTemplate()
    {
        ExcelUtil<PpmSiteSource> util = new ExcelUtil<PpmSiteSource>(PpmSiteSource.class);
        return util.importTemplateExcel("站点来源数据");
    }

    /**
     * 返回所有市/区县Json
     */
    @ApiOperation("省平台返回所有市/区县Json")
    @GetMapping("/getAllSite")
    public AjaxResult getAllSiteJson()
    {
        Map<String, List<String>> siteJsonMap = ppmSiteSourceService.getAllSite();
        return AjaxResult.success(siteJsonMap);
    }

    /**
     * 导出来源站点列表
     */
    //@PreAuthorize("@ss.hasPermi('system:source:export')")
    @Log(title = "来源站点", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PpmSiteSource ppmSiteSource)
    {
        List<PpmSiteSource> list = ppmSiteSourceService.selectPpmSiteSourceList(ppmSiteSource);
        ExcelUtil<PpmSiteSource> util = new ExcelUtil<PpmSiteSource>(PpmSiteSource.class);
        return util.exportExcel(list, "source");
    }

    /**
     * 获取来源站点详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:source:query')")
    @GetMapping(value = "/{siteSourceId}")
    public AjaxResult getInfo(@PathVariable("siteSourceId") Long siteSourceId)
    {
        return AjaxResult.success(ppmSiteSourceService.selectPpmSiteSourceById(siteSourceId));
    }

    /**
     * 新增来源站点
     */
    //@PreAuthorize("@ss.hasPermi('system:source:add')")
    @Log(title = "来源站点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PpmSiteSource ppmSiteSource)
    {
        ppmSiteSource.setCreateBy(SecurityUtils.getUsername());
        ppmSiteSource.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(ppmSiteSourceService.insertPpmSiteSource(ppmSiteSource));
    }

    /**
     * 修改来源站点
     */
    @ApiOperation("省平台修改来源站点")
    //@PreAuthorize("@ss.hasPermi('system:source:edit')")
    @Log(title = "来源站点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PpmSiteSource ppmSiteSource)
    {
        ppmSiteSource.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(ppmSiteSourceService.updatePpmSiteSource(ppmSiteSource));
    }

    /**
     * 删除来源站点
     */
    //@PreAuthorize("@ss.hasPermi('system:source:remove')")
    @Log(title = "来源站点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{siteSourceIds}")
    public AjaxResult remove(@PathVariable Long[] siteSourceIds)
    {
        return toAjax(ppmSiteSourceService.deletePpmSiteSourceByIds(siteSourceIds));
    }
}
