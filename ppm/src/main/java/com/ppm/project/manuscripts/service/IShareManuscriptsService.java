package com.ppm.project.manuscripts.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ppm.project.manuscripts.domain.ShareManuscripts;

/**
 * 共享稿件管理Service接口
 * 
 * @author ppm
 * @date 2020-04-03
 */
public interface IShareManuscriptsService extends IService<ShareManuscripts>
{
    /**
     * 查询共享稿件管理
     * 
     * @param id 共享稿件管理ID
     * @return 共享稿件管理
     */
    public ShareManuscripts selectShareManuscriptsById(Long id);

    /**
     * 查询共享稿件管理列表
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 共享稿件管理集合
     */
    public List<ShareManuscripts> selectShareManuscriptsList(ShareManuscripts shareManuscripts);

    /**
     * 新增共享稿件管理
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 结果
     */
    public int insertShareManuscripts(ShareManuscripts shareManuscripts);

    /**
     * 修改共享稿件管理
     * 
     * @param shareManuscripts 共享稿件管理
     * @return 结果
     */
    public int updateShareManuscripts(ShareManuscripts shareManuscripts);

    /**
     * 批量删除共享稿件管理
     * 
     * @param ids 需要删除的共享稿件管理ID
     * @return 结果
     */
    public int deleteShareManuscriptsByIds(Long[] ids);

    /**
     * 删除共享稿件管理信息
     * 
     * @param id 共享稿件管理ID
     * @return 结果
     */
    public int deleteShareManuscriptsById(Long id);
}
