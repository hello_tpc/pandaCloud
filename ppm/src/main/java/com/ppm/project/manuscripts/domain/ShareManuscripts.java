package com.ppm.project.manuscripts.domain;

import com.ppm.framework.aspectj.lang.annotation.Excel;
import com.ppm.framework.web.domain.server.NonEmptyBaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 共享稿件管理对象 ppm_manuscripts
 * 
 * @author ppm
 * @date 2020-04-03
 */
public class ShareManuscripts extends NonEmptyBaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 文章ID，区县代码+新闻ID */
    @Excel(name = "文章ID，区县代码+新闻ID")
    private String articleId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String title;

    /** 来源代码 */
    @Excel(name = "来源代码")
    private String sourceFrom;

    /** 是否共享区县，1是0否 */
    @Excel(name = "是否共享区县，1是0否")
    private Integer shareFlag;

    /** 新闻H5地址 */
    @Excel(name = "新闻H5地址")
    private String h5Html;

    /*** 来源名称 */
    private String sourceFromName;

    /*** 引用次数 */
    @Excel(name = "引用次数")
    private Integer refCount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setArticleId(String articleId) 
    {
        this.articleId = articleId;
    }

    public String getArticleId() 
    {
        return articleId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setSourceFrom(String sourceFrom) 
    {
        this.sourceFrom = sourceFrom;
    }

    public String getSourceFrom() 
    {
        return sourceFrom;
    }
    public void setShareFlag(Integer shareFlag) 
    {
        this.shareFlag = shareFlag;
    }

    public Integer getShareFlag() 
    {
        return shareFlag;
    }
    public void setH5Html(String h5Html) 
    {
        this.h5Html = h5Html;
    }

    public String getH5Html() 
    {
        return h5Html;
    }

    public String getSourceFromName() {
        return sourceFromName;
    }

    public void setSourceFromName(String sourceFromName) {
        this.sourceFromName = sourceFromName;
    }

    public Integer getRefCount() { return refCount; }

    public void setRefCount(Integer refCount) { this.refCount = refCount; }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("articleId", getArticleId())
            .append("title", getTitle())
            .append("sourceFrom", getSourceFrom())
            .append("shareFlag", getShareFlag())
            .append("h5Html", getH5Html())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
