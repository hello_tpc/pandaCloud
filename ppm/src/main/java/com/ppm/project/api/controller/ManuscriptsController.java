package com.ppm.project.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ppm.common.utils.StringUtils;
import com.ppm.common.utils.poi.ExcelUtil;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;
import com.ppm.framework.web.page.TableDataInfo;
import com.ppm.project.api.domain.Manuscripts;
import com.ppm.project.api.service.IManuscriptsService;
import com.ppm.project.manuscripts.domain.PpmSiteSource;
import com.ppm.project.manuscripts.service.IPpmSiteSourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 共享文稿库Controller
 * 
 * @author ppm
 * @date 2020-04-01
 */
@Api(description = "CMS共享稿件接口相关",value = "CMS共享稿件接口",tags = {"CMS共享稿件接口"})
@RestController
@RequestMapping("/api/sharemanuscripts")
public class ManuscriptsController extends BaseController
{
    @Autowired
    private IManuscriptsService manuscriptsService;

    @Autowired
    private IPpmSiteSourceService ppmSiteSourceService;


    /**
     * 查询共享文稿库列表
     */
    @ApiOperation("查询共享文稿库列表")
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:list')")
    @GetMapping("/getManuscripts")
    public TableDataInfo getManuscripts(Manuscripts manuscripts,String siteCode)
    {
        startPage();
        manuscripts.setCurrentSiteCode(siteCode);
        if(!StringUtils.isEmpty(manuscripts.getSearchValue())){
            manuscripts.setSearchValue("%"+manuscripts.getSearchValue()+"%");
        }
        List<Manuscripts> list = manuscriptsService.selectManuscriptsList(manuscripts);
        PpmSiteSource retPpmSiteSource = ppmSiteSourceService.getOne(new QueryWrapper<PpmSiteSource>()
                .eq("site_code" , siteCode));
        if(retPpmSiteSource != null && retPpmSiteSource.getShareFlag() != 1){
            //不共享直接返回空
            list = new ArrayList<>();
        }

        Map<String, List<String>> allSite = ppmSiteSourceService.getAllSite();
        TableDataInfo dataTable = getDataTable(list);
        if(allSite != null){
            //返站点
            dataTable.setParams("allSite",allSite);
        }
        if(retPpmSiteSource != null){
            //返积分
            dataTable.setParams("integral",retPpmSiteSource.getIntegral());
        }
        return dataTable;
    }

    /**
     * 导出省平台共享文稿库列表
     */
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:export')")
    @Log(title = "省平台共享文稿库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Manuscripts manuscripts)
    {
        List<Manuscripts> list = manuscriptsService.selectManuscriptsList(manuscripts);
        ExcelUtil<Manuscripts> util = new ExcelUtil<Manuscripts>(Manuscripts.class);
        return util.exportExcel(list, "manuscripts");
    }

    /**
     * 获取省平台共享文稿库详细信息
     */
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:query')")
    @GetMapping(value = "getManuscriptsById/{id}")
    public AjaxResult getManuscriptsById(@PathVariable("id") Long id)
    {
        return AjaxResult.success(manuscriptsService.selectManuscriptsById(id));
    }

    /**
     * 新增省平台共享文稿库
     */
    @ApiOperation("新增共享文稿库")
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:add')")
    @Log(title = "省平台共享文稿库", businessType = BusinessType.INSERT)
    @PostMapping("/addManuscripts")
    public AjaxResult addManuscripts(@RequestBody List<Manuscripts> manuscriptsList)
    {
        boolean allSuccessFlag = manuscriptsService.batchInsertManuscripts(manuscriptsList);

        return toAjax(allSuccessFlag ?1:0);
    }

    /**
     * 修改省平台共享文稿库
     */
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:edit')")
    @Log(title = "省平台共享文稿库", businessType = BusinessType.UPDATE)
    @PostMapping("/updateManuscriptsById")
    public AjaxResult updateManuscriptsById(Manuscripts manuscripts)
    {
        if(manuscripts.getId() == null){
            return AjaxResult.error("[id]不可为空");
        }
        return toAjax(manuscriptsService.updateManuscripts(manuscripts));
    }

    /**
     * 删除省平台共享文稿库
     */
    @ApiOperation("批量删除共享文稿库")
    //@PreAuthorize("@ss.hasPermi('ppm:manuscripts:remove')")
    @Log(title = "省平台共享文稿库", businessType = BusinessType.DELETE)
	@PostMapping("deleteManuscriptsByIds")
    public AjaxResult deleteManuscriptsByIds(@RequestBody List<String> ids)
    {
        return toAjax(manuscriptsService.deleteManuscriptsByIds(ids));
    }
}
