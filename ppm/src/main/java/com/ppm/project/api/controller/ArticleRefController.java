package com.ppm.project.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ppm.framework.aspectj.lang.annotation.Log;
import com.ppm.framework.aspectj.lang.enums.BusinessType;
import com.ppm.project.manuscripts.domain.PpmArticleRef;
import com.ppm.project.manuscripts.service.IPpmArticleRefService;
import com.ppm.framework.web.controller.BaseController;
import com.ppm.framework.web.domain.AjaxResult;

/**
 * 文章引用Controller
 * 
 * @author ppm
 * @date 2020-04-03
 */
@RestController
@RequestMapping("/api/ref")
public class ArticleRefController extends BaseController
{
    @Autowired
    private IPpmArticleRefService ppmArticleRefService;

    /**
     * 新增文章引用
     */
//    @PreAuthorize("@ss.hasPermi('manuscripts:ref:add')")
    @Log(title = "文章引用", businessType = BusinessType.INSERT)
    @PostMapping("/addArticleRef")
    public AjaxResult add(@RequestBody PpmArticleRef ppmArticleRef)
    {
        return toAjax(ppmArticleRefService.insertPpmArticleRef(ppmArticleRef));
    }
}
