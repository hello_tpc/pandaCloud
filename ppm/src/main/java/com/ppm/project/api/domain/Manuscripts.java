package com.ppm.project.api.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ppm.framework.aspectj.lang.annotation.Excel;
import com.ppm.framework.web.domain.server.NonEmptyBaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 省平台共享文稿库对象 ppm_manuscripts
 * 
 * @author ppm
 * @date 2020-04-01
 */

@TableName("ppm_manuscripts")
public class Manuscripts extends NonEmptyBaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 文章ID，区县代码+新闻ID */
    @TableId(value = "article_id")
    @Excel(name = "文章ID，区县代码+新闻ID")
    private String articleId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String title;

    /** 来源代码 */
    @Excel(name = "来源代码")
    private String sourceFrom;

    /** 是否共享区县，1是0否 */
    @Excel(name = "是否共享区县，1是0否")
    private Integer shareFlag;

    /** 新闻H5地址 */
    @Excel(name = "新闻H5地址")
    private String h5Html;

    @TableField(exist = false)
    /*** 来源名称 */
    private String sourceFromName;

    @TableField(exist = false)
    /*** 当前站点 */
    private String currentSiteCode;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setArticleId(String articleId)
    {
        this.articleId = articleId;
    }

    public String getArticleId()
    {
        return articleId;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setSourceFrom(String sourceFrom)
    {
        this.sourceFrom = sourceFrom;
    }

    public String getSourceFrom()
    {
        return sourceFrom;
    }
    public void setShareFlag(Integer shareFlag)
    {
        this.shareFlag = shareFlag;
    }

    public Integer getShareFlag()
    {
        return shareFlag;
    }

    public void setH5Html(String h5Html)
    {
        this.h5Html = h5Html;
    }

    public String getH5Html()
    {
        return h5Html;
    }

    public String getSourceFromName() {
        return sourceFromName;
    }

    public void setSourceFromName(String sourceFromName) {
        this.sourceFromName = sourceFromName;
    }

    public String getCurrentSiteCode() {
        return currentSiteCode;
    }

    public void setCurrentSiteCode(String currentSiteCode) {
        this.currentSiteCode = currentSiteCode;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("articleId", getArticleId())
            .append("title", getTitle())
            .append("sourceFrom", getSourceFrom())
            .append("shareFlag", getShareFlag())
            .append("h5Html", getH5Html())
            .append("sourceFromName", getSourceFromName())
            .append("currentSiteCode", getCurrentSiteCode())
            .toString();
    }
}
