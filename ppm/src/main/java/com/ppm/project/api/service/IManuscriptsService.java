package com.ppm.project.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ppm.project.api.domain.Manuscripts;

import java.util.List;

/**
 * 省平台共享文稿库Service接口
 * 
 * @author ppm
 * @date 2020-04-01
 */
public interface IManuscriptsService extends IService<Manuscripts>
{
    /**
     * 查询省平台共享文稿库
     * 
     * @param id 省平台共享文稿库ID
     * @return 省平台共享文稿库
     */
    public Manuscripts selectManuscriptsById(Long id);

    /**
     * 查询省平台共享文稿库列表
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 省平台共享文稿库集合
     */
    public List<Manuscripts> selectManuscriptsList(Manuscripts manuscripts);

    /**
     * 新增省平台共享文稿库
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 结果
     */
    public int insertManuscripts(Manuscripts manuscripts);

    /**
     * 批量新增省平台共享文稿库
     *
     * @param manuscriptsList
     * @return
     */

    public boolean batchInsertManuscripts(List<Manuscripts> manuscriptsList);

    /**
     * 修改省平台共享文稿库
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 结果
     */
    public int updateManuscripts(Manuscripts manuscripts);

    /**
     * 批量删除省平台共享文稿库
     * 
     * @param ids 需要删除的省平台共享文稿库ID
     * @return 结果
     */
    public int deleteManuscriptsByIds(List<String> ids);

    /**
     * 删除省平台共享文稿库信息
     * 
     * @param id 省平台共享文稿库ID
     * @return 结果
     */
    public int deleteManuscriptsById(Long id);
}
