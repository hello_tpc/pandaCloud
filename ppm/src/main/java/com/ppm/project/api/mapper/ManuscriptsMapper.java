package com.ppm.project.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ppm.project.api.domain.Manuscripts;

import java.util.List;
import java.util.Set;

/**
 * 省平台共享文稿库Mapper接口
 * 
 * @author ppm
 * @date 2020-04-01
 */
public interface ManuscriptsMapper extends BaseMapper<Manuscripts>
{
    /**
     * 查询省平台共享文稿库
     * 
     * @param id 省平台共享文稿库ID
     * @return 省平台共享文稿库
     */
    public Manuscripts selectManuscriptsById(Long id);

    /**
     * 查询省平台共享文稿库列表
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 省平台共享文稿库集合
     */
    public List<Manuscripts> selectManuscriptsList(Manuscripts manuscripts);

    /**
     * 批量查询sourceFrom
     *
     * @param articleIds
     * @return sourceFrom集合
     */
    public List<String> selectExistArticleId(Set<String> articleIds);

    /**
     * 新增省平台共享文稿库
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 结果
     */
    public int insertManuscripts(Manuscripts manuscripts);

    /**
     * 修改省平台共享文稿库
     * 
     * @param manuscripts 省平台共享文稿库
     * @return 结果
     */
    public int updateManuscripts(Manuscripts manuscripts);

    /**
     * 删除省平台共享文稿库
     * 
     * @param id 省平台共享文稿库ID
     * @return 结果
     */
    public int deleteManuscriptsById(Long id);

    /**
     * 批量删除省平台共享文稿库
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteManuscriptsByIds(List<String> ids);
}
