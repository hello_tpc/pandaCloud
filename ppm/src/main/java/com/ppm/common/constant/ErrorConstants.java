package com.ppm.common.constant;

/**
 * 错误状态码
 */
public class ErrorConstants {

    public static final Integer DB_UNIQUEKEY_CHECK_ERROR = 40001;
    public static final Integer CONCURRENT_ERROR = 40002;
}
