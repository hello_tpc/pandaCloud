package com.ppm.common.exception.user;


public class ArticleIdUniqueCheckException extends RuntimeException {

    private Integer code;
    private String message;

    public ArticleIdUniqueCheckException(String message) {
        super(message);
    }

    public ArticleIdUniqueCheckException(Integer code , String message , Throwable t) {

        super(message, t);
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
