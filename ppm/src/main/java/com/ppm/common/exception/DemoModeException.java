package com.ppm.common.exception;

/**
 * 演示模式异常
 * 
 * @author ppm
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
