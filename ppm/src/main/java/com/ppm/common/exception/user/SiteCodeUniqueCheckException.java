package com.ppm.common.exception.user;


public class SiteCodeUniqueCheckException extends RuntimeException {

    private Integer code;
    private String message;

    public SiteCodeUniqueCheckException(String message) {
        super(message);
    }

    public SiteCodeUniqueCheckException(Integer code , String message , Throwable t) {

        super(message, t);
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
