package com.ppm.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author ppm
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
