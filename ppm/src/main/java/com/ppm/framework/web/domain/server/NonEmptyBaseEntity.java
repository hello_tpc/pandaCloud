package com.ppm.framework.web.domain.server;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ppm.framework.web.domain.BaseEntity;

/**
 * 字段为空 Json数据中不显示
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NonEmptyBaseEntity extends BaseEntity {
}
