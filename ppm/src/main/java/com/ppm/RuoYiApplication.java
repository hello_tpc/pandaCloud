package com.ppm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author ppm
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class RuoYiApplication
{
    public static void main(String[] args)
    {
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(RuoYiApplication.class, args);
        System.out.println("    ___     ___  __  __    ___     _       ___    _   _    ___   \n" +
                "   | _ \\   | _ \\|  \\/  |  / __|   | |     / _ \\  | | | |  |   \\  \n" +
                "   |  _/   |  _/| |\\/| | | (__    | |__  | (_) | | |_| |  | |) | \n" +
                "  _|_|_   _|_|_ |_|__|_|  \\___|   |____|  \\___/   \\___/   |___/  \n" +
                "_| \"\"\" |_| \"\"\" |_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"| \n" +
                "\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-' \n"
        +"启动成功");
    }
}
