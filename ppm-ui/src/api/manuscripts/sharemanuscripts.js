import request from '@/utils/request'

// 查询共享稿件管理列表
export function listSharemanuscripts(query) {
  return request({
    url: '/manuscripts/sharemanuscripts/list',
    method: 'get',
    params: query
  })
}


// 查询共享稿件管理详细
export function getSharemanuscripts(id) {
  return request({
    url: '/manuscripts/sharemanuscripts/' + id,
    method: 'get'
  })
}

// 新增共享稿件管理
export function addSharemanuscripts(data) {
  return request({
    url: '/manuscripts/sharemanuscripts',
    method: 'post',
    data: data
  })
}

// 修改共享稿件管理
export function updateSharemanuscripts(data) {
  return request({
    url: '/manuscripts/sharemanuscripts',
    method: 'put',
    data: data
  })
}

// 删除共享稿件管理
export function delSharemanuscripts(id) {
  return request({
    url: '/manuscripts/sharemanuscripts/' + id,
    method: 'delete'
  })
}

// 导出共享稿件管理
export function exportSharemanuscripts(query) {
  return request({
    url: '/manuscripts/sharemanuscripts/export',
    method: 'get',
    params: query
  })
}
