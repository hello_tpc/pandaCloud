import request from '@/utils/request'

// 查询城市和区县选项数据
export function getAllAreaData() {
  return request({
    url: '/manuscripts/source/getAllSite',
    method: 'get',
  })
}

// 查询来源站点列表
export function listSource(query) {
  return request({
    url: '/manuscripts/source/list',
    method: 'get',
    params: query
  })
}

// 查询来源站点详细
export function getSource(id) {
  return request({
    url: '/manuscripts/source/' + id,
    method: 'get'
  })
}

// 新增来源站点
export function addSource(data) {
  return request({
    url: '/manuscripts/source',
    method: 'post',
    data: data
  })
}

// 修改来源站点
export function updateSource(data) {
  return request({
    url: '/manuscripts/source',
    method: 'put',
    data: data
  })
}

// 删除来源站点
export function delSource(id) {
  return request({
    url: '/manuscripts/source/' + id,
    method: 'delete'
  })
}

// 导出来源站点
export function exportSource(query) {
  return request({
    url: '/manuscripts/source/export',
    method: 'get',
    params: query
  })
}

// 下载站点来源配置导入模板
export function downloadTemplate() {
  return request({
    url: '/manuscripts/source/importTemplate',
    method: 'get'
  })
}
