import request from '@/utils/request'

// 查询共享素材管理列表
export function listSharematerial(query) {
  return request({
    url: '/manuscripts/sharemanuscripts/list',
    method: 'get',
    params: query
  })
}

// 查询稿件素材列表
export function listManuscriptMaterial(query) {
  return request({
    url: '/manuscripts/sharematerial/list',
    method: 'get',
    params: query
  })
}

// 同步素材
export function syncMaterial(data) {
  return request({
    url: '/manuscripts/sharematerial/syncMaterial',
    method: 'post',
    data: data
  })
}
