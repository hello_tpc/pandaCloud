import request from '@/utils/request'

// 查询文章引用列表
export function listRef(query) {
  return request({
    url: '/manuscripts/ref/list',
    method: 'get',
    params: query
  })
}

// 查询文章引用详细
export function getRef(id) {
  return request({
    url: '/manuscripts/ref/' + id,
    method: 'get'
  })
}

// 新增文章引用
export function addRef(data) {
  return request({
    url: '/manuscripts/ref',
    method: 'post',
    data: data
  })
}

// 修改文章引用
export function updateRef(data) {
  return request({
    url: '/manuscripts/ref',
    method: 'put',
    data: data
  })
}

// 删除文章引用
export function delRef(id) {
  return request({
    url: '/manuscripts/ref/' + id,
    method: 'delete'
  })
}

// 导出文章引用
export function exportRef(query) {
  return request({
    url: '/manuscripts/ref/export',
    method: 'get',
    params: query
  })
}